package info.hccis.admin.dao;

import info.hccis.ojt.model.Business;
import info.hccis.ojt.model.DatabaseConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author nicholaspeconi
 * @since 10/20/2018
 * @purpose This class is used to access the business table in the cis2232_ojt
 * database and perfrom various methods to modify the DB
 */
public class BusinessDAO {

    /**
     * @author nicholaspeconi
     * @since 10/20/2018
     * @purpose This method is used to insert an array of businesses into the DB
     * @param businesses
     */
    public void insertBusinesses(ArrayList<Business> businesses) {

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        // new DB connectiom object created
        DatabaseConnection databaseConnection = new DatabaseConnection();
        try {
            // connect to the database
            conn = ConnectionUtils.getDBConnection(databaseConnection);

            // for each business in the array 
            for (Business current : businesses) {
                // boolean to see if username already exists
                boolean found = false;
                // look through the DB for the username of the current business
                try {
                    sql = "Select `username` from `Business` WHERE `username`= ?";
                    ps = conn.prepareStatement(sql);
                    ps.setString(1, current.getUsername());
                    ResultSet rs = ps.executeQuery();

                    // if the username is found trip boolean to true
                    while (rs.next()) {
                        if (rs.getString("username").equalsIgnoreCase(current.getUsername())) {
                            found = true;
                        }
                    }
                    // SQl errors in Select statement
                } catch (Exception e) {
                    String errorMessage = e.getMessage();
                    e.printStackTrace();
                }
                // if the username was not found than add them to the database
                if (!found) {
                    try {
                        sql = "INSERT INTO `Business`(`username`, `name`, `phoneNumber`) VALUES (?,?,?)";
                        ps = conn.prepareStatement(sql);

                        // setting the values of the current business into SQL statement
                        ps.setString(1, current.getUsername());
                        ps.setString(2, current.getName());
                        ps.setString(3, current.getPhoneNumber());
                        ps.execute();
                        // SQl errors in the Insert statement
                    } catch (Exception e) {
                        String errorMessage = e.getMessage();
                        e.printStackTrace();
                    }
                }
            }
//            connecting to DB errors
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            // close DB connection
            DbUtils.close(ps, conn);
        }
    }
}
