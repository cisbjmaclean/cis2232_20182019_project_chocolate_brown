package info.hccis.admin.dao;

import info.hccis.ojt.model.DatabaseConnection;
import info.hccis.ojt.model.Student;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * Student DAO
 *
 * @author Darcy Brown
 * @date Oct 15, 2018
 */
public class StudentDAO {

    public static int count = 0;

    /**
     * This is the connect method. allows me to make a database connection by
     * declaring a StudentDAO Object.
     *
     * @author Darcy Brown
     * @date Oct 15th, 2018
     */
    public static synchronized int insertStudent(DatabaseConnection databaseConnection, Student student) throws Exception {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        boolean results = true;
        boolean exist = false;

        // Check to see if the exist first, if they do make exist true.
        try {
            conn = ConnectionUtils.getDBConnection(databaseConnection);

            sql = "SELECT `lastName`,`firstName` FROM `Student`";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                if (rs.getString("firstName").equalsIgnoreCase(student.getFirstName())
                        && rs.getString("lastName").equalsIgnoreCase(student.getLastName())) {
                    exist = true;
                    count = 0;
                }
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        // only if exist is false, do insert.
        if (!exist) {
            try {
                conn = ConnectionUtils.getDBConnection(databaseConnection);
                sql = "INSERT INTO `Student`(`studentId`, `firstName`, `lastName`)"
                        + "VALUES (?,?,?)";

                ps = conn.prepareStatement(sql);
                ps.setInt(1, Integer.parseInt(student.getStudentId()));
                ps.setString(2, student.getFirstName());
                ps.setString(3, student.getLastName());
                ps.execute();
                count++;

            } catch (Exception e) {
                String errorMessage = e.getMessage();
                e.printStackTrace();
                results = false;
                throw e;

            } finally {
                DbUtils.close(ps, conn);
            }
            return count;
        } else {
            return count;
        }

    }

    public static ArrayList<Student> selectAllByResumeAndCoverLetterSubmit() {

        ArrayList<Student> students = new ArrayList();
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        try {
            DatabaseConnection databaseConnection = new DatabaseConnection();
            conn = ConnectionUtils.getDBConnection(databaseConnection);

            sql = "SELECT * FROM Student WHERE coverLetterSubmittedDate > ''"
                    + "AND resumeSubmittedDate > ''";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                int id = rs.getInt("id");
                String studentId = rs.getString("studentId");
                String lastName = rs.getString("lastName");
                String firstName = rs.getString("firstName");
                String coverLetterCubmittedDate = rs.getString("coverLetterSubmittedDate");
                String resumeSubmittedDate = rs.getString("resumeSubmittedDate");
                Student student = new Student(id, studentId, lastName, firstName,
                        coverLetterCubmittedDate, resumeSubmittedDate);

                students.add(student);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        return students;

    }
    
    
    
    public static ArrayList<Student> getStudents(DatabaseConnection databaseConnection) {
        ArrayList<Student> students = new ArrayList();

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getDBConnection(databaseConnection);

            sql = "SELECT * FROM `Student` order by id";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Student student = new Student();
                student.setId(rs.getInt("id"));
                student.setUsername(rs.getString("username"));
                student.setLastName(rs.getString("lastName"));
                student.setFirstName(rs.getString("firstName"));
                student.setStudentId(rs.getString("studentId"));
                student.setPhone(rs.getString("phone"));
                student.setResumeLocation(rs.getString("resumeLocation"));
                student.setPreferredTypeOfWork(rs.getString("preferredTypeOfWork"));
                student.setCoverLetterSubmittedDate(rs.getString("coverLetterSubmittedDate"));
                student.setResumeSubmittedDate(rs.getString("resumeSubmittedDate"));
                students.add(student);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return students;
    }

}
