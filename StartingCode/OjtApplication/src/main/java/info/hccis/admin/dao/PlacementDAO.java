package info.hccis.admin.dao;

import info.hccis.ojt.model.DatabaseConnection;
import info.hccis.ojt.util.PlacementUtil;
import info.hccis.ojt.util.Utility;
import info.hccis.user.bo.PlacementResults;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Placement DAO
 *
 * @author Jennifer Townsend
 * @since Oct. 25th, 2018
 */
public class PlacementDAO {

    private final static Logger LOGGER = Logger.getLogger(PlacementDAO.class.getName());
    public static String FILE_NAME;
    private static boolean appendToFile = false;

    public static ArrayList<String> placementSelect() {

        //create an arrayList to hold information gathered from the database
        ArrayList<String> placements = new ArrayList();
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        try {
            conn = ConnectionUtils.getDBConnection(databaseConnection);
            sql = "SELECT `lastName`, `firstName`, `name` FROM `Student`,`Placement`, `Business` where Student.id = Placement.studentId AND Business.id = Placement.businessId";

            ps = conn.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                String lastName = rs.getString("lastName");
                String firstName = rs.getString("firstName");
                String business = rs.getString("name");
                String dataToAdd = "First Name: " + firstName + " Last Name: " + lastName
                        + " Name of Business: " + business;

                placements.add(dataToAdd);
            }

        } catch (Exception e) {

            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        try {
            PlacementUtil.writeToFile(placements);
        } catch (IOException ex) {
            Logger.getLogger(PlacementDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return placements;
    }

    public static ArrayList<PlacementResults> placementList() {

        ArrayList<PlacementResults> aPlacementResult = new ArrayList();
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        try {
            conn = ConnectionUtils.getDBConnection(databaseConnection);
            sql = "SELECT Placement.id, `lastName`, `firstName`, `name`, `notes`, `placementDate` FROM `Student`,`Placement`, `Business` where Student.id = Placement.studentId AND Business.id = Placement.businessId";

            ps = conn.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int placementId = rs.getInt("Placement.id");
                String lastName = rs.getString("lastName");
                String firstName = rs.getString("firstName");
                String business = rs.getString("name");
                String notes = rs.getString("notes");
                String date = rs.getString("placementDate");

                PlacementResults placementResults = new PlacementResults(placementId, firstName, lastName, business, notes, date);
                aPlacementResult.add(placementResults);

            }

        } catch (Exception e) {

            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return aPlacementResult;
    }

    public static synchronized void delete(int id) throws Exception {

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        try {
            conn = ConnectionUtils.getDBConnection(databaseConnection);

            sql = "DELETE FROM `Placement` WHERE id=?";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();

        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            throw e;
        } finally {
            DbUtils.close(ps, conn);
        }
    }

    public static ArrayList<PlacementResults> studentPlacementReport() {

        ArrayList<PlacementResults> studentNotInPlacement = new ArrayList();
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        try {
            conn = ConnectionUtils.getDBConnection(databaseConnection);
            sql = "SELECT `firstName`, `lastName`, `Id` FROM Student where `Id` NOT IN (SELECT `studentId` FROM Placement)";

            ps = conn.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int studentId = rs.getInt("Id");
                String lastName = rs.getString("lastName");
                String firstName = rs.getString("firstName");

                PlacementResults placementResults = new PlacementResults(studentId, lastName, firstName);
                //placementResults.setStudentId(studentId);
                //placementResults.setStudentLastName(lastName);
               // placementResults.setStudentFirstName(firstName);
                studentNotInPlacement.add(placementResults);

            }

        } catch (Exception e) {

            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return studentNotInPlacement;
    }

    public static ArrayList<PlacementResults> businessPlacementReport() {

        ArrayList<PlacementResults> businessNotInPlacement = new ArrayList();
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        try {
            conn = ConnectionUtils.getDBConnection(databaseConnection);
            sql = "SELECT `name`, `Id` FROM Business where `Id` NOT IN (SELECT `businessId` FROM Placement)";

            ps = conn.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int businessId = rs.getInt("Id");
                String businessName = rs.getString("name");

                PlacementResults placementResults = new PlacementResults(businessId, businessName);
                //placementResults.setBusinessId(businessId);
                //placementResults.setBusinessName(businessName);

                businessNotInPlacement.add(placementResults);

            }

        } catch (Exception e) {

            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return businessNotInPlacement;
    }
}
