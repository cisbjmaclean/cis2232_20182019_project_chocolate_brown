/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.user.bo;

import info.hccis.ojt.model.Placement;

/**
 *
 * @author Jennifer Townsend
 * @since Nov. 9th 2018
 */
public class PlacementResults extends Placement {

    private int placementId;
    private String studentFirstName;
    private String studentLastName;
    private String businessName;
    

    public PlacementResults() {

    }

    public PlacementResults(int placementId, String studentFirstName, String studentLastName, String businessName, String notes, String placementDate) {

        this.placementId = placementId;
        this.studentFirstName = studentFirstName;
        this.studentLastName = studentLastName;
        this.businessName = businessName;
        super.setNotes(notes);
        super.setPlacementDate(placementDate);
    }

    public PlacementResults(int studentId, String studentLastName, String studentFirstName) {
        super.setStudentId(studentId);
        this.studentFirstName = studentFirstName;
        this.studentLastName = studentLastName;
    }

    public PlacementResults(int businessId, String businessName) {
        super.setBusinessId(businessId);
        this.businessName = businessName;
    }

    public int getPlacementId() {
        return placementId;
    }

    public void setPlacementId(int placementId) {
        this.placementId = placementId;
    }

    public String getStudentFirstName() {
        return studentFirstName;
    }

    public void setStudentFirstName(String studentFirstName) {
        this.studentFirstName = studentFirstName;
    }

    public String getStudentLastName() {
        return studentLastName;
    }

    public void setStudentLastName(String studentLastName) {
        this.studentLastName = studentLastName;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

}
