package info.hccis.ojt.model;

import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class will be a pojo which contains the database name, userName, and
 * password. It can be used when this information is needed.
 *
 * @author bjmaclean
 * @since 20180524
 */
public class DatabaseConnection {

    private String databaseName;
    private String userName;
    private String password;

    /**
     * Create a DatabaseConnection object using the parameter values.
     *
     * @author bjmaclean
     * @since 20180524
     *
     * @param databaseName Name of the database
     * @param userName Username
     * @param password Password
     */
    public DatabaseConnection(String databaseName, String userName, String password) {
        this.databaseName = databaseName;
        this.userName = userName;
        this.password = password;
    }

    /**
     * Create a DatabaseConnection object which obtains the credentials from the
     * data-access.properties config file.
     *
     * @author bjmaclean
     * @since 20180524
     *
     */
    public DatabaseConnection() {
        try {
            String propFileName = "spring.data-access";
            ResourceBundle rb = ResourceBundle.getBundle(propFileName);
            userName = rb.getString("jdbc.username");
            password = rb.getString("jdbc.password");
            databaseName = rb.getString("jdbc.dbname");
            System.out.println("BJM-Set the database to " + databaseName);

        } catch (Exception ex) {
            System.out.println("PROBLEM!!!!!");
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, "Unable to get default database information");
        }
    }

    @Override
    public String toString() {
        return "DatabaseConnection{" + "databaseName=" + databaseName + ", userName=" + userName + ", password=" + password + '}';
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
