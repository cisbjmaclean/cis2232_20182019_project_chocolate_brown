/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.ojt.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author nicholaspeconi
 */
@Entity
@Table(name = "Business")
@XmlRootElement
public class Business implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Size(max = 100)
    @Column(name = "username")
    private String username;
    @Size(max = 50)
    @Column(name = "name")
    private String name;
    @Size(max = 10)
    @Column(name = "phoneNumber")
    private String phoneNumber;
    @Size(max = 100)
    @Column(name = "website")
    private String website;
    @Size(max = 10)
    @Column(name = "confirmLetterSentDate")
    private String confirmLetterSentDate;
    @Size(max = 10)
    @Column(name = "liabilityIdemnificationSentDate")
    private String liabilityIdemnificationSentDate;
    @Column(name = "numberOfPositions")
    private Integer numberOfPositions;

    public Business() {
    }

    public Business(Integer id, String username, String phoneNumber, String name) {
        this.id = id;
        this.username = username;
        this.phoneNumber = phoneNumber;
        this.name = name;
    }
    
     public Business(String username, String phoneNumber, String name) {
        this.username = username;
        this.phoneNumber = phoneNumber;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getConfirmLetterSentDate() {
        return confirmLetterSentDate;
    }

    public void setConfirmLetterSentDate(String confirmLetterSentDate) {
        this.confirmLetterSentDate = confirmLetterSentDate;
    }

    public String getLiabilityIdemnificationSentDate() {
        return liabilityIdemnificationSentDate;
    }

    public void setLiabilityIdemnificationSentDate(String liabilityIdemnificationSentDate) {
        this.liabilityIdemnificationSentDate = liabilityIdemnificationSentDate;
    }

    public Integer getNumberOfPositions() {
        return numberOfPositions;
    }

    public void setNumberOfPositions(Integer numberOfPositions) {
        this.numberOfPositions = numberOfPositions;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Business)) {
            return false;
        }
        Business other = (Business) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.camper.model.Business[ id=" + id + " ]";
    }
    
}
