package info.hccis.ojt.web;

import info.hccis.ojt.data.springdatajpa.BusinessRepository;
import info.hccis.ojt.data.springdatajpa.UserRepository;
import info.hccis.ojt.model.Business;
import info.hccis.ojt.model.jpa.User;
import info.hccis.ojt.util.BusinessUtil;
import info.hccis.user.bo.UserBO;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/*
 *@author: Nick Peconi
 *@since: October 20 2018
 *@purpose: This file is a business controller that will control the actions with regards to the Business objects between the model,view, and data access controls of this system
 *
 */
@Controller
public class BusinessController {

    private final BusinessRepository br;
    private final OtherController oc;
    private final UserRepository ur;

    // autowire JPA Repository
    @Autowired
    public BusinessController(BusinessRepository br, OtherController oc, UserRepository ur) {
        this.br = br;
        this.oc = oc;
        this.ur = ur;
    }

//     Request mapping called from the bodyheader menu
    @RequestMapping("/business/businesscsv")
    public String readBusinessCsv(HttpServletRequest request) {

   
        String filePath = "/cis2232/businessList.csv";
        //create a temp businessUtil object
        BusinessUtil tempUtil = new BusinessUtil();

        // sets the file path in the businessUtil object
        tempUtil.setFilePath(filePath);
        try {
            tempUtil.readCSV();
        } catch (IOException ex) {
            Logger.getLogger(BusinessController.class.getName()).log(Level.SEVERE, null, ex);
        }
        // return the about page for now
        return "business/businessImportSuccess";
    }

    @RequestMapping("/business/list")
    public String businessList(Model model, @ModelAttribute("user") User user, HttpSession session) {

        //Get the businesses from the database
        user = (User) session.getAttribute("loggedInUser");
        if (user.getUserTypeCode() == 1) {
            model.addAttribute("businesses", br.findAll());
            return "business/list";
        } else {
            String username = (String) session.getAttribute("username");
            System.out.println("NP - Logged In Username = " + username);

            // Update using jpa
            model.addAttribute("businesses", br.findOneByUsername(username));
            return "/business/list";
        }

        //This will send the user to the list page
    }

    @RequestMapping("/business/add")
    public String businessAdd(Model model) {

        //put a business object in the model to be used to associate with the input tags of 
        //the form.
        Business newBusiness = new Business();
        newBusiness.setId(0);
        model.addAttribute("business", newBusiness);

        //This will send the user to the welcome.html page.
        return "business/add";
    }

    @RequestMapping("/business/addSubmit")
    public String businessAddSubmit(Model model, @Valid @ModelAttribute("business") Business theBusinessFromTheForm, BindingResult result, HttpSession session, User user) {

        if (result.hasErrors()) {
            System.out.println("Error in validation.");
            String error = "Error";
            model.addAttribute("message", error);
            return "business/add";
        }

        try {
            br.save(theBusinessFromTheForm);
        } catch (Exception e) {
            System.out.println("Could not save to the database");
        }

        user = (User) session.getAttribute("loggedInUser");

        if (user.getUserTypeCode() == 1) {
            model.addAttribute("businesses", br.findAll());
            return "business/list";
        } else {
            String username = (String) session.getAttribute("username");
            System.out.println("NP - Logged In Username = " + username);

            model.addAttribute("businesses", br.findOneByUsername(username));
            return "/business/list";
        }

    }

    @RequestMapping("/business/delete")
    public String businessDelete(Model model, HttpServletRequest request) {

        //- This method will use the id which is passed as a request 
        //parameter.  
        String idToFind = request.getParameter("id");
        System.out.println("BJM id passed=" + idToFind);

        try {
            br.delete(Integer.parseInt(idToFind));
        } catch (Exception ex) {
            System.out.println("Could not delete");
        }
        //Reload the businesses list so it can be shown on the next view.
        model.addAttribute("businesses", br.findAll());
        return "business/list";

    }

    @RequestMapping("/business/update")
    public String businessUpdate(Model model, HttpServletRequest request) {

        //- This method will use the registration id which is passed as a request 
        //parameter.  
        String idToFind = request.getParameter("id");
        System.out.println("BJM id passed=" + idToFind);

        Business editBusiness = br.findOne(Integer.parseInt(idToFind));

        model.addAttribute("business", editBusiness);
        return "/business/add";

    }

    /*
 
     */
    @RequestMapping("/business/report")
    public String businessReport(Model model, HttpSession session) {

        List<Business> listConfirmNull = br.findByConfirmLetterSentDate(null);
        List<Business> listConfirmEmpty = br.findByConfirmLetterSentDate("");
        List<Business> listLiabilityNull = br.findByLiabilityIdemnificationSentDate(null);
        List<Business> listLiabilityEmpty = br.findByLiabilityIdemnificationSentDate("");

        List<Business> newList = new ArrayList<>(listConfirmNull);
        for (Business current : listLiabilityNull) {
            if (!newList.contains(current)) {
                newList.add(current);
            }
        }
        for (Business current : listConfirmEmpty) {
            if (!newList.contains(current)) {
                newList.add(current);
            }
        }
        for (Business current : listLiabilityEmpty) {
            if (!newList.contains(current)) {
                newList.add(current);
            }
        }

        model.addAttribute("businesses", newList);

        //This will send the user to the list page
        return "business/report";
    }

}
