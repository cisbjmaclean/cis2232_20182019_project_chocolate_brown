package info.hccis.ojt.web;
import info.hccis.admin.dao.StudentDAO;
import info.hccis.ojt.data.springdatajpa.StudentRepository;
import info.hccis.ojt.model.Student;
import info.hccis.ojt.util.StudentUtil;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

 /**
    * Student controller.
    * The main controller for the student parts of this application.
    * @author Darcy Brown
     * @throws java.lang.Exception
    * @date Oct 17, 2018
    */



@Controller
public class StudentController {
    
    private final StudentRepository sr;
    
    // autowire JPA Repository
    @Autowired
    public StudentController(StudentRepository sr, OtherController oc) {
        this.sr = sr;
    }
    
    /**
     * CSV Loading Controller
     * @param model
     * @param request
     * @return
     * @throws Exception 
     * 
     * @author: Darcy Brown
     * @since: Nov 3rd, 2018
     */
    @RequestMapping("/student/studentcsv")
    public String studentCSV(Model model,HttpServletRequest request) throws Exception {
        int count = 0;
        // Use these lines to link ot the CSV folder in webpages/resources to eliviate file location problems
        StudentUtil tempUtil = new StudentUtil();  
        tempUtil.readCSV();
        count = tempUtil.readCSV();
        
        model.addAttribute("count",count);
        count = 0;
    return "student/importsuccessful";
    }
    
    /**
     * Student Add Controller
     * @param model
     * @return 
     * 
     * @author: Darcy Brown
     * @since: Nov 3rd, 2018
     */
     @RequestMapping("/student/add")
    public String studentAdd(Model model) {

        //put a camper object in the model to be used to associate with the input tags of 
        //the form.
        Student student = new Student();
        student.setId(0);
        model.addAttribute("student", student);

        //This will send the user to the welcome.html page.
        return "student/add";
    }
    
    /**
     * Student Update Controller
     * @param model
     * @param request
     * @return 
     * 
     * @author: Darcy Brown
     * @since: Nov 3rd, 2018
     */
    @RequestMapping("/student/update")
    public String studentUpdate(Model model, HttpServletRequest request) {

        //- This method will use the registration id which is passed as a request 
        //parameter.  
        String idToFind = request.getParameter("id");
        System.out.println("DAB id passed=" + idToFind);

        
        // Update using jpa
        Student student = sr.findOne(Integer.parseInt(idToFind));

        
        model.addAttribute("student", student);
        return "student/add";

    }
    
     @RequestMapping("/student/updateself")
    public String studentUpdateSelf(Model model,HttpSession session) {

        //- This method will use the registration id which is passed as a request 
        //parameter.  
        String username = (String) session.getAttribute("username");
        System.out.println("DAB - Logged In Username = " + username);

        // get student
        Student student = sr.findOneByUsername(username);
        
        // if null student change message
        if(student == null){
           model.addAttribute("message", "No student with this username in system.");
        } else {
           model.addAttribute("message", "Please enter updated information."); 
        }
          
        // add student attribute.
        model.addAttribute("student", student);
        return "student/updateself";
    }
    
    /**
     * Student Delete Controller
     * @param model
     * @param request
     * @return
     * @throws Exception 
     * 
     * @author: Darcy Brown
     * @since: Nov 3rd, 2018
     */
        @RequestMapping("/student/delete")
    public String studentDelete(Model model, HttpServletRequest request) throws Exception {

        //- This method will use the registration id which is passed as a request 
        //parameter.  
        String idToDelete = request.getParameter("id");
        System.out.println("DAB id passed=" + idToDelete);

        // Delete using JPA
        sr.delete(Integer.parseInt(idToDelete));

        model.addAttribute("students", sr.findAll());
        return "student/list";

    }
    
    /**
     * Student Add Submit Controller
     * @param model
     * @param studentFromForm
     * @param result
     * @return 
     * 
     * @author: Darcy Brown
     * @since: Nov 3rd, 2018
     */
    
      @RequestMapping("/student/selfsubmit")
    public String studentSelfSubmit(Model model, @Valid @ModelAttribute("student") Student studentFromForm, BindingResult result,HttpSession session) {

        
        if (result.hasErrors()) {
            System.out.println("Error in validation.");
            return "student/updateself";
        }

        //Call the dao method to put this guy in the database.
        try {
            // JPA save reflection
            sr.save(studentFromForm);
        } catch (Exception e) {
            System.out.println("Could not save to the database");
        }

        //- This method will use the registration id which is passed as a request 
        //parameter.  
        String username = (String)session.getAttribute("username");
        System.out.println("DAB - Logged In Username = " + username);

        // Update using jpa
        model.addAttribute("student", sr.findOneByUsername(username));
        model.addAttribute("message","Update Successful!");
        //This will send the user to the welcome.html page.
        return "student/updateself";
    }
    
     @RequestMapping("/student/addSubmit")
    public String studentAddSubmit(Model model, @Valid @ModelAttribute("student") Student studentFromForm, BindingResult result) {

        
        if (result.hasErrors()) {
            System.out.println("Error in validation.");
            return "student/add";
        }

        //Call the dao method to put this guy in the database.
        try {
            // JPA save reflection
            sr.save(studentFromForm);
        } catch (Exception e) {
            System.out.println("Could not save to the database");
        }

        System.out.println("DAB-Did we get here?");
        //Reload the campers list so it can be shown on the next view.
        model.addAttribute("students", sr.findAll());
        //This will send the user to the welcome.html page.
        return "student/list";
    }
    
    /*
    * Student List Controller
    */
     @RequestMapping("/student/list")
    public String studentList(Model model) {

      
        //Get the campers from the database
        //model.addAttribute("campers", CamperDAO.selectAll());
        model.addAttribute("students", sr.findAll());

        //This will send the user to the list page
        return "student/list";
    }
    
     /*
    * Student Submission Report Controller
    */
     @RequestMapping("/student/subreport")
    public String studentSubmissionReport(Model model, HttpSession session) {
        //Get the campers from the database
        //model.addAttribute("campers", CamperDAO.selectAll());
        model.addAttribute("students", StudentDAO.selectAllByResumeAndCoverLetterSubmit());

        //This will send the user to the list page
        return "student/subreport";
    }
    
}
