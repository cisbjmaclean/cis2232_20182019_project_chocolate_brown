package info.hccis.ojt.data.springdatajpa;

import info.hccis.ojt.model.jpa.User;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

    List<User> findByUsername(String username);

}
