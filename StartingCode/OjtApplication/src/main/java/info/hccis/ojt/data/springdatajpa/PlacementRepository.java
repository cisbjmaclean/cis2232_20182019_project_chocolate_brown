package info.hccis.ojt.data.springdatajpa;

import info.hccis.ojt.model.Placement;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlacementRepository extends CrudRepository<Placement, Integer> {

    //Can add specific methods such as these
    
    List<Placement> findByStudentId(Integer studentId);
    List<Placement> findByBusinessId(Integer businessId);
    List<Placement> findByPlacementDate(String placementDate);
   

}
