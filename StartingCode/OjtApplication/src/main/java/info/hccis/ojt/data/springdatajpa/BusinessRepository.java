
package info.hccis.ojt.data.springdatajpa;

import info.hccis.ojt.model.Business;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author darcy
 * @date Nov 2, 2018
 */


@Repository
public interface BusinessRepository extends CrudRepository<Business, Integer> {

    //Can add specific methods such as these
    List<Business> findByName(String name);
    List<Business> findByUsername(String username);
    List<Business> findByPhoneNumber(String phoneNumber);
    List<Business> findByWebsite(String website);
    List<Business> findByNumberOfPositions(Integer numberOfPositions);
    List<Business> findByConfirmLetterSentDate(String confirmLetterSentDate);
    List<Business> findByLiabilityIdemnificationSentDate(String liabilityIdemnificationSentDate);

    Business findOneByUsername(String username);
}