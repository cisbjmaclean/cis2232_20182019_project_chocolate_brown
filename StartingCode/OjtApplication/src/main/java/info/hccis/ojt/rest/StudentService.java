package info.hccis.ojt.rest;

import com.google.gson.Gson;
import info.hccis.admin.dao.StudentDAO;
import info.hccis.ojt.data.springdatajpa.StudentRepository;
import info.hccis.ojt.model.DatabaseConnection;
import info.hccis.ojt.model.Student;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

@Path("/StudentService")
public class StudentService {
    
    @Resource
    private final StudentRepository sr;

    /**
     * Note that dependency injection (constructor) is used here to provide the UserRepository 
     * object for use in this class.  
     * 
     * @param servletContext Context
     * @since 20180604
     * @author BJM
     */
    
    public StudentService(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.sr = applicationContext.getBean(StudentRepository.class);
    }

    /**
     * This rest service will provide all students from the associated database.
     * @return json string containing all user information.
     * @since 20181109
     * @author DAB
     */
    
    @GET
    @Path("/students") // Don't know why this works and /students doesn't
    @Produces(MediaType.APPLICATION_JSON)
    public Response getStudents() {

        ArrayList<Student> students = StudentDAO.getStudents(new DatabaseConnection());
//        ArrayList<Student> students = (ArrayList<Student>) sr.findAll();
        Gson gson = new Gson();

        int statusCode = 200;
        if (students.isEmpty()) {
            statusCode = 204;
        }

        String temp = "";
        temp = gson.toJson(students);

        return Response.status(statusCode).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }
    
    @GET
    @Path("/students/{username}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getStudent(@PathParam("username") String studentUser) {

        ArrayList<Student> student = (ArrayList<Student>) sr.findByUsername(studentUser);

        System.out.println("Getting Student info for username="+studentUser);
        
        Gson gson = new Gson();
        //convert the user to JSON
        String temp;
        //check if any errors occurred
        int statusCode = 200;
        if (student.isEmpty()) {
            //if the logins array is empty, or the password doesn't match, return a new null user
            temp = gson.toJson(new Student());
        } else {
            //if the passwords match, then return the 
            // testing match
           
            System.out.println("DEBUG db username " + student.get(0).getUsername());
            temp = gson.toJson(student.get(0));
        }

        return Response.status(statusCode).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
    }
}
