package info.hccis.ojt.rest;

import java.util.ArrayList;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

@Path("/api")
public class ApiService {

    public ApiService(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
    }

    @GET
    @Path("/")
    @Produces(MediaType.TEXT_HTML)
    public Response getServices() {
        ArrayList<String> services = new ArrayList();

        String output = "<html><body><h1>Services available</h1>";
        output += "<h2>StudentService</h2>";
        output += "<p>Get all students</p>";
        output += "<p>example:   <a target=\"_blank\" href=\"http://hccis.info:8080/ojt/rest/StudentService/students\">http://hccis.info:8080/ojt/rest/StudentService/students</a></p>";
        output += "<p>Get a particular student</p>";
        output += "<p><a target=\"_blank\" href=\"http://hccis.info:8080/ojt/rest/StudentService/students/jsmith@hollandcollege.com\">http://hccis.info:8080/ojt/rest/StudentService/students/jsmith@hollandcollege.com</a></p>";
        output += "<h2>BusinessService</h2>";
        output += "<p>Get all businesses</p>";
        output += "<p><a target=\"_blank\" href=\"http://hccis.info:8080/ojt/rest/BusinessService/businesses\">http://hccis.info:8080/ojt/rest/BusinessService/businesses</a></p>";
        output += "<p>Get a particular business /business/business/name</p>";
        output += "<p><a target=\"_blank\" href=\"http://hccis.info:8080/ojt/rest/BusinessService/business/Holland+College\">http://hccis.info:8080/ojt/rest/BusinessService/business/Holland+College</a></p>";
        output += "<h2>PlacementService</h2>";
        output += "<p><a target=\"_blank\" href=\"http://hccis.info:8080/ojt/rest/PlacementService/placements\">http://hccis.info:8080/ojt/rest/PlacementService/placements</a></p>";
        output += "<p>Get businesses without OJT students  /placementBusiness</p>";
        output += "<p><a target=\"_blank\" href=\"http://localhost:8080/ojt/rest/PlacementService/placementBusiness\">http://localhost:8080/ojt/rest/PlacementService/placementBusiness</a></p>";
        output += "<h2>UserService</h2>";
        output += "<p>Login user (returns user object as json) /user/userName/hashedPassword</p>";
        output += "<p><a target=\"_blank\" href=\"http://hccis.info:8080/ojt/rest/UserService/users/bmaclean/202cb962ac59075b964b07152d234b70\">http://hccis.info:8080/ojt/rest/UserService/users/bmaclean/202cb962ac59075b964b07152d234b70</a></p>";
        output += "<p>Get all users  /users</p>";
        output += "<p><a target=\"_blank\" href=\"http://hccis.info:8080/ojt/rest/UserService/users\">http://hccis.info:8080/ojt/rest/UserService/users</a></p><body></html>";

        return Response.status(200).entity(output).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
    }
}
