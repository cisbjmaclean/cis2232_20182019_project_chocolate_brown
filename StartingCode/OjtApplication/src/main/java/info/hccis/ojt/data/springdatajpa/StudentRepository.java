
package info.hccis.ojt.data.springdatajpa;

import info.hccis.ojt.model.Student;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author darcy
 * @date Nov 2, 2018
 */


@Repository
public interface StudentRepository extends CrudRepository<Student, Integer> {

    //Can add specific methods such as these
    List<Student> findByLastName(String lastName);
    List<Student> findByFirstName(String firstName);
    List<Student> findByStudentId(String studentId);
    List<Student> findByFirstNameAndLastName(String firstName, String lastName);
    List<Student> findByPreferredTypeOfWork(String preferredTypeOfWork);
    List<Student> findByUsername(String username);
    Student findOneByUsername(String username);
}