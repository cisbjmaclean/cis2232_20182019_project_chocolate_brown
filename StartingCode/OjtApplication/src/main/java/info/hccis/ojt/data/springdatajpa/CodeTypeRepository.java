package info.hccis.ojt.data.springdatajpa;

import info.hccis.ojt.model.jpa.CodeType;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CodeTypeRepository extends CrudRepository<CodeType, Integer> {

    List<CodeType> findByCreatedUserId(String createdID);
    List<CodeType> findByEnglishDescription(String englishDescription);
}