package info.hccis.ojt.rest;

import com.google.gson.Gson;
import info.hccis.ojt.data.springdatajpa.BusinessRepository;
import info.hccis.ojt.model.DatabaseConnection;
import info.hccis.ojt.model.Business;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

@Path("/BusinessService")
public class BusinessService {

    @Resource
    private final BusinessRepository br;

    /**
     * Note that dependency injection (constructor) is used here to provide the
     * UserRepository object for use in this class.
     *
     * @param servletContext Context
     * @since 20180604
     * @author BJM
     */
    public BusinessService(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.br = applicationContext.getBean(BusinessRepository.class);
    }

    /**
     * This rest service will provide all users from the associated database.
     *
     * @return json string containing all user information.
     * @since 20180604
     * @author BJM
     */
    @GET
    @Path("/businesses")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBusinesses() {

        ArrayList<Business> businesses = (ArrayList) br.findAll();
        Gson gson = new Gson();

        int statusCode = 200;
        if (businesses.isEmpty()) {
            statusCode = 204;
        }

        String temp = "";
        temp = gson.toJson(businesses);


        return Response.status(statusCode).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }

    /**
     * This rest service will provide all a user object from the associated
     * database that has a matching username/password (hashed)
     *
     * @param businessName
     * @return json string containing the user attributes.
     * @since 20180604
     * @author BJM
     */
    @GET
    @Path("/business/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBusiness(@PathParam("name") String businessName) {

        ArrayList<Business> businesses = (ArrayList<Business>) br.findByName(businessName);

        Gson gson = new Gson();
        //convert the user to JSON
        String temp;
        //check if any errors occurred
        int statusCode = 200;
        if (businesses.isEmpty()) {
            //if the businesses array is empty return a new null business
            temp = "There is no Business with that business name Try Again!";
        } else {

            temp = gson.toJson(businesses);

        }

        return Response.status(statusCode).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
    }

}
