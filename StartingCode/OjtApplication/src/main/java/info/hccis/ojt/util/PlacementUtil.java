package info.hccis.ojt.util;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Gets the current date and adds it to the name of the file; writes an
 * arrayList of strings to a file found in a cis2232/ojt folder.
 *
 * @author Jennifer Townsend
 * @since Oct 25, 2018
 */
public class PlacementUtil {

    public static String FILE_NAME;
    private static boolean appendToFile = false;

    public static void writeToFile(ArrayList<String> writePlacementToFile) throws IOException {

        String currentDate = Utility.getNow("yyyyMMdd");
        FILE_NAME = "/cis2232/ojt_" + currentDate + ".txt ";
        FileWriter write = new FileWriter(FILE_NAME, appendToFile);
        PrintWriter printLine = new PrintWriter(write);

        for (String placement : writePlacementToFile) {
            printLine.printf("%s" + "%n", placement);

        }
        printLine.close();
    }
}
