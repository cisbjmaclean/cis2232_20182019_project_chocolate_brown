package info.hccis.ojt.rest;

import com.google.gson.Gson;
import info.hccis.admin.dao.PlacementDAO;
import info.hccis.ojt.data.springdatajpa.PlacementRepository;
import info.hccis.ojt.model.DatabaseConnection;
import info.hccis.ojt.model.Placement;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

@Path("/PlacementService")
public class PlacementService {

    @Resource
    private final PlacementRepository pr;

    /**
     * Note that dependency injection (constructor) is used here to provide the
     * UserRepository object for use in this class.
     *
     * @param servletContext Context
     * @since 20180604
     * @author BJM
     */
    public PlacementService(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.pr = applicationContext.getBean(PlacementRepository.class);
    }

    /**
     * This rest service will provide all users from the placement table
     * database.
     *
     * @return json string containing all placement information.
     * @since 20181119
     * @author JTownsend
     */
    @GET
    @Path("/placements")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPlacements() {

        ArrayList<Placement> placements = (ArrayList) PlacementDAO.placementList();
        Gson gson = new Gson();

        int statusCode = 200;
        if (placements.isEmpty()) {
            statusCode = 204;
        }

        String temp = "";
        temp = gson.toJson(placements);

        return Response.status(statusCode).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
    }

    /**
     * This rest service will provide all businesses without an OJT student
     *
     * @return json string containing the user attributes.
     * @since 20181119
     * @author JTownsend
     */
    @GET
    @Path("/placementBusiness")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPlacementBusinesses() {

        ArrayList<Placement> placements = (ArrayList) PlacementDAO.businessPlacementReport();
        Gson gson = new Gson();

        int statusCode = 200;
        if (placements.isEmpty()) {
            statusCode = 204;
        }

        String temp = "";
        temp = gson.toJson(placements);

        return Response.status(statusCode).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
    }
}
