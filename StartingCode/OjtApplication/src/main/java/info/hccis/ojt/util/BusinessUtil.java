package info.hccis.ojt.util;

import info.hccis.admin.dao.BusinessDAO;
import info.hccis.ojt.model.Business;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/*
 * 
 *@author: Nick Peconi
 *@since: October 20 2018
 *@purpose: This file will contain the utility methods that are called from the business controller
 *
 *
 */
public class BusinessUtil {

    // Path oc CSV file goes here
    private String filePath = "";
    // array list of businesses to be added to the database
    private ArrayList<Business> businesses = new ArrayList();

    // Default constructor
    public BusinessUtil() {
    }

    /**
     * @author: Nick Peconi
     * @since: October 20 2018
     * @purpose sets the file path passed from Business controller
     * @param filePath
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * @purpose this method will call the copyCsvBusinesses method that will
     * populate the arraylist then call the insertbusinesses method
     * @throws IOException
     */
    public void readCSV() throws IOException {
        businesses = copyCsvBusinesses();
        insertBusinnesses(businesses);
    }

    /*
    *@author: Nick Peconi
    *@since: October 20 2018
    *@purpose: this method will read from a csv file and then populate an arraylist of business objects
     @return: tempBusinesses arraylist    
     */
    private ArrayList<Business> copyCsvBusinesses() throws IOException {
        ArrayList<Business> tempBusinesses = new ArrayList<>();

        try {
            //BufferedReader to read from file
            BufferedReader bReader = new BufferedReader(new FileReader(filePath));
            
            String line = bReader.readLine();
            // do until there no more lines
            while (line != null) {
                // string array that is split at the commas in the csv
                String[] businessCsvEntry = line.split(",");
                // create a business object using the values from the string array
                Business tempBusiness = new Business(businessCsvEntry[0], businessCsvEntry[1], businessCsvEntry[2]);

                tempBusinesses.add(tempBusiness);
                line = bReader.readLine();

            }
            if (bReader == null) {
                bReader.close();
            }

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        return tempBusinesses;
    }

    /**
     * @author: Nick Peconi
     * @since: October 20 2018
     * @purpose: this method will accept an array list and then call the insertBusinesses method of the business DAO
     * @param businesses
     */
    public void insertBusinnesses(ArrayList<Business> businesses) {
        BusinessDAO tempDAO = new BusinessDAO();
        tempDAO.insertBusinesses(businesses);

    }
}
