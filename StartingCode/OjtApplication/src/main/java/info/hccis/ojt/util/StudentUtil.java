package info.hccis.ojt.util;
import info.hccis.admin.dao.StudentDAO;
import info.hccis.ojt.model.DatabaseConnection;
import info.hccis.ojt.model.Student;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;


/**
 * This takes all the stuff from the CSV file and makes an array of objects.
 * It then loops through the array putting them into the database.
 * It uses the Student class and the StudentDAO class.
 * 
 * @author darcy
 * @date Oct 15, 2018
 * 
 */
public class StudentUtil {
     public static int count = 0;

     // Path oc CSV file goes here
     private String filePath = "C:\\cis2232\\studentList.csv";
     private ArrayList<Student> students = new ArrayList();
    
     
     //filepath getters and setters
    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
       
    }
    
   public int getCount() {
        return count;
    }

    public void setCount(int countInput) {
        count = countInput;
    }
     
    
    public int readCSV() throws Exception{
        count = 0;
         // Load the students from CSV into a list
        students = readStudents(filePath);
        setCount(count+=insertStudents(students));
        return count;
    }

    /**
     * This is my readStudents method that uses a file reader and buffered reader
     * to read all the lines. It uses a String array and the split function to split
     * the lines at the delimiter into separate strings. It then makes a temp student
     * stores it in the temp array then returns it.
     * 
     * @author: Darcy Brown
     * @since October 15h,2018
     * 
     * @param filePath
     * @return 
     */
    private ArrayList<Student> readStudents(String filePath) {
        
        // make an array lsit to store the laoded csv file into
        ArrayList<Student> studentTemp = new ArrayList<>();
       
        try (
                
            // Use buffered reader to read lines
            FileReader csvFile = new FileReader(filePath);
            BufferedReader br = new BufferedReader(csvFile)) {
            String line = br.readLine();

            // loop until all lines are read
            while (line != null) {
                
                // Found this line on stack overflow
                String[] studentAttributes = line.split(",");
                
                // make a student object with those studentAttributes add to array
                Student student = new Student(studentAttributes[0],studentAttributes[1],studentAttributes[2]);
                studentTemp.add(student);
                // next line
                line = br.readLine();
            }

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        
        // return the array list of students
        return studentTemp;
    }
    
    
    private int insertStudents(ArrayList<Student> students) throws SQLException, Exception{
        
        // create a StudentDAO studentdao
       StudentDAO studentdao = new StudentDAO();
       DatabaseConnection connection = new DatabaseConnection();
       
       // for each student insertStudent
       for(Student current: students){
           count = studentdao.insertStudent(connection,current);   
       } 
       return count;
    }


}