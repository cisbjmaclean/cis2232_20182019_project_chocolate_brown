package info.hccis.ojt.web;

import static com.sun.corba.se.spi.presentation.rmi.StubAdapter.request;
import info.hccis.admin.dao.PlacementDAO;
import info.hccis.ojt.data.springdatajpa.BusinessRepository;
import info.hccis.ojt.data.springdatajpa.PlacementRepository;
import info.hccis.ojt.data.springdatajpa.StudentRepository;
import info.hccis.ojt.model.Business;
import info.hccis.ojt.model.Placement;
import info.hccis.ojt.model.Student;
import info.hccis.ojt.util.PlacementUtil;
import info.hccis.user.bo.PlacementResults;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Placement controller. This gathers information from the ojt database and
 * writes it to a file.
 *
 * @author Jennifer Townsend
 * @throws java.lang.Exception
 * @since Oct 25, 2018
 */
@Controller
public class PlacementController {

    private final PlacementRepository pr;
    private final StudentRepository sr;
    private final BusinessRepository br;

    @Autowired
    public PlacementController(PlacementRepository pr, StudentRepository sr, BusinessRepository br) {

        this.pr = pr;
        this.sr = sr;
        this.br = br;

    }

    /*
    * Student List Controller
     */
    @RequestMapping("/placement/list")
    public String placementList(Model model, HttpServletRequest request) {
        
        
        
        model.addAttribute("placements", PlacementDAO.placementList());
        

        //This will send the user to the list page
        return "placement/list";
    }

    @RequestMapping("/placement/delete")
    public String placementDelete(Model model, HttpServletRequest request) {

        
       
        String idToFind = request.getParameter("id");
       
        int id = Integer.parseInt(idToFind);
       
        try {
            PlacementDAO.delete(id);
        } catch (Exception ex) {
            Logger.getLogger(PlacementController.class.getName()).log(Level.SEVERE, null, ex);
        }
        model.addAttribute("placements", PlacementDAO.placementList());
        return "placement/list";

    }

    @RequestMapping("/placement/add")
    public String placementAdd(Model model, HttpSession session) {

        
        Placement newPlacement = new Placement();
        newPlacement.setId(0);
        session.setAttribute("students", sr.findAll());
        session.setAttribute("businesses", br.findAll());
        model.addAttribute("placement", newPlacement);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
	LocalDate localDate = LocalDate.now();
        newPlacement.setPlacementDate(dtf.format(localDate));
        //session.setAttribute("date",dtf.format(localDate));
	
        //This will send the user to the welcome.html page.
        return "placement/add";
    }

    @RequestMapping("/placement/addSubmit")
    public String placementAddSubmit(Model model, @Valid @ModelAttribute("placement") Placement placementFromForm, BindingResult result) {

        if (result.hasErrors()) {
            System.out.println("Error in validation.");
            return "placement/add";
        }

        try {

            pr.save(placementFromForm);
        } catch (Exception e) {
            System.out.println("Could not save to the database");
        }

        System.out.println("JT-Did we get here?");
        //Reload the campers list so it can be shown on the next view.
        model.addAttribute("placements", PlacementDAO.placementList());
        //This will send the user to the placement.list page.
        return "placement/list";
    }
    
    
    @RequestMapping("/placement/studentPlacementReport")
    public String studentPlacementReport(Model model, HttpServletRequest request) {
        
        
        
        model.addAttribute("placements", PlacementDAO.studentPlacementReport());
        

        //This will send the user to the list page
        return "placement/studentPlacementReport";
    }

    @RequestMapping("/placement/businessPlacementReport")
    public String businessPlacementReport(Model model, HttpServletRequest request) {
        
        
        
        model.addAttribute("businesses", PlacementDAO.businessPlacementReport());
        

        //This will send the user to the list page
        return "placement/businessPlacementReport";
    }
    @RequestMapping("/placement/export")
    public String showPlacementExport(Model model) throws IOException {
        //get the student name and business name from the placment table; write it to a file
        ArrayList<String> myResults = PlacementDAO.placementSelect();
        try {
            PlacementUtil.writeToFile(myResults);
        } catch (IOException ex) {
            Logger.getLogger(PlacementDAO.class.getName()).log(Level.SEVERE, null, ex);
            return "placement/writtenToFileNotSuccessful";
        }
        return "placement/writtenToFileSuccessful";
    }
}
