/**
 * This is a Business test file; it is used to test the attributes in the Business class
 *
 * @author Nick Peconi
 * @since December 5th, 2018
 */
package info.hccis.admin.util;

import info.hccis.ojt.model.Business;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author npeconi
 */
public class BusinessTest {

    private Business businessTest;

    public BusinessTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
 /**
     * This tests the getUsername method in the business class
     * 
     * @author: Nick Peconi
     * @since December 5th, 2018
     */
    @Test
    public void testBusinessGetUsername() {
        String result = "npeconi@hollandcollege.com";

        businessTest = new Business();
        businessTest.setUsername(result);
        assertEquals("npeconi@hollandcollege.com", businessTest.getUsername());

    }
  /**
     * This tests the getBusinessName method in the business class
     * 
     * @author: Nick Peconi
     * @since December 5th, 2018
     */
    @Test
    public void testBusinessGetName() {
        String result = "UPEI";

        businessTest = new Business();
        businessTest.setName(result);
        assertEquals("UPEI", businessTest.getName());

    }
  /**
     * This tests the getPhoneNumber method in the business class
     * 
     * @author: Nick Peconi
     * @since December 5th, 2018
     */
    @Test
    public void testBusinessGetPhoneNumber() {
        String result = "9023307734";

        businessTest = new Business();
        businessTest.setPhoneNumber(result);
        assertEquals("9023307734", businessTest.getPhoneNumber());

    }
 /**
     * This tests the getWebsite method in the business class
     * 
     * @author: Nick Peconi
     * @since December 5th, 2018
     */
    @Test
    public void testBusinessGetWebsite() {
        String result = "nick.com";

        businessTest = new Business();
        businessTest.setWebsite(result);
        assertEquals("nick.com", businessTest.getWebsite());

    }
 /**
     * This tests the getId method in the business class
     * 
     * @author: Nick Peconi
     * @since December 5th, 2018
     */
    @Test
    public void testBusinessGetId() {
        Integer result = 1234;

        businessTest = new Business();
        businessTest.setId(result);
        
        assertEquals((Integer)1234,businessTest.getId());

    }
}
