/**
 * This is a placement test file; it is used to test the attributes in the Placement class
 *
 * @author Jennifer
 * @since December 2nd, 2018
 */
package info.hccis.admin.util;

import info.hccis.ojt.model.Placement;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PlacementTest {

    private Placement placement = new Placement();

    public PlacementTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test for the Id attribute of a Placement object
     *
     * @author: Jennifer Townsend
     * @since December 2nd, 2018
     */
    @Test
    public void placementGetId() {
        placement = new Placement();
        placement.setId(4);
        assertEquals((Integer) 4, placement.getId());
    }

    /**
     * Test for the studentId attribute of a Placement object
     *
     * @author: Jennifer Townsend
     * @since December 2nd, 2018
     */
    @Test
    public void placementGetStudentId() {
        placement = new Placement();
        placement.setStudentId(54);
        assertEquals((Integer) 54, placement.getStudentId());
    }

    /**
     * Test for the businessId attribute of a Placement object
     *
     * @author: Jennifer Townsend
     * @since December 2nd, 2018
     */
    @Test
    public void placementGetBusinessId() {
        placement = new Placement();
        placement.setBusinessId(32);
        assertEquals((Integer) 32, placement.getBusinessId());
    }

    /**
     * Test for the placementDate attribute of a Placement object
     *
     * @author: Jennifer Townsend
     * @since December 2nd, 2018
     */
    @Test
    public void placementGetPlacementDate() {
        placement = new Placement();
        placement.setPlacementDate("20180606");
        assertEquals("20180606", placement.getPlacementDate());
    }

    /**
     * Test for the notes attribute of a Placement object
     *
     * @author: Jennifer Townsend
     * @since December 2nd, 2018
     */
    @Test
    public void placementGetNotes() {
        placement = new Placement();
        placement.setNotes("This is a test");
        assertEquals("This is a test", placement.getNotes());
    }

}
