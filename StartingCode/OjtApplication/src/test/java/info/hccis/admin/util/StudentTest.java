/**
 * This is a test file to test a few getters and setters for the Student Object.
 * 
 * @author Darcy Brown
 * @since November 28th, 2018
 */
package info.hccis.admin.util;

import info.hccis.ojt.model.Student;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class StudentTest {
    
    private Student testStudent;
    
    public StudentTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * This tests the First name get and set for a Student object.
     * 
     * @author: Brown
     * @since November 28th, 2018
     */
   @Test
   public void Student_Test_GetSet_Firstname(){
        testStudent = new Student();
        testStudent.setFirstName("TestUserFirstName");
        assertEquals("TestUserFirstName",testStudent.getFirstName());   
    }
   
    /**
     * This tests the Last name get and set for a Student object.
     * 
     * @author: Brown
     * @since November 28th, 2018
     */
   @Test
   public void Student_Test_GetSet_Lastname(){
        testStudent = new Student();
        testStudent.setFirstName("TestUserLastName");
        assertEquals("TestUserLastName",testStudent.getFirstName());   
    }
   
    /**
     * This tests the Student ID get and set for a Student object.
     * 
     * @author: Brown
     * @since November 28th, 2018
     */
   @Test
   public void Student_Test_GetSet_StudentID(){
        testStudent = new Student();
        testStudent.setStudentId("123456");
        assertEquals("123456",testStudent.getStudentId());   
    }
   
    /**
     * This tests the ID get and set for a Student object.
     * 
     * @author: Brown
     * @since November 28th, 2018
     */
   @Test
   public void Student_Test_GetSet_ID(){
        testStudent = new Student();
        testStudent.setId(12345);
        assertEquals((Integer)12345,testStudent.getId());   
    }
   
    /**
     * This tests the Phone number get and set for a Student object.
     * 
     * @author: Brown
     * @since November 28th, 2018
     */
   @Test
   public void Student_Test_GetSet_Phone(){
        testStudent = new Student();
        testStudent.setPhone("19023150982");
        assertEquals("19023150982",testStudent.getPhone());   
    }
   
}
